class ChainyardAPI {
  async fetchLatestBlock() {
    return await this.get('latestblock')
  }

  async mockLatestBlock() {
    return new Promise((resolve, reject) => {
      resolve({
        "hash":"0000000000000538200a48202ca6340e983646ca088c7618ae82d68e0c76ef5a",
        "time":1325794737,
        "block_index":841841,
        "height":160778,
        "txIndexes":[13950369,13950510,13951472]
      })
   }, 100);
  }

  async fetchBlockByHash(hash) {
    const endpoint = `rawblock/${hash}`;

    return await this.get(endpoint)
  }

  async fetchTransaction(tx_hash) {
    const endpoint = `rawtx/${tx_hash}`

    return await this.get(endpoint)
  }

  // Fetch blocks for one day
  // Pass time in milliseconds
  // Must specify format as json through query &format=json
  async fetchBlocksByDate(time_in_ms) {
    console.log(time_in_ms)
    const endpoint = `blocks/${time_in_ms.valueOf()}?format=json`

    return await this.get(endpoint)
  }


  // Fetch blocks by height
  // I thought block height was unique
  // but the api gives me an array for some reason
  // maybe it counts unconfirmed blocks as well in different branches
  // But pretty sure should return one
  // requires json format query
  async fetchBlocksByHeight(height) {
    const endpoint = `block-height/${height}?format=json`

    return await this.get(endpoint)
  }

  // Get Fetch request, returns response body as json
  // Does not catch errors
  get(endpoint) {
    const API_DOMAIN = "https://blockchain.info"

    const url = `${API_DOMAIN}/${endpoint}`

    return fetch(url, { mode: 'cors', credentials: 'omit' })
      .then(res => res.json());
  }
}

export default new ChainyardAPI()

