import React, { Component } from 'react';

import { BrowserRouter as Router, Route } from 'react-router-dom'
import './App.css';

import BlocksListPage from './BlocksListPage'
import BlockDetailPage from './BlockDetailPage'
import TransactionDetailPage from './TransactionDetailPage'

class App extends Component {
  render() {
    return (
      <div className="App">
        <Router>
          <div>
            <header>
              <nav className="navbar navbar-expand-lg bg-dark">
                <a className="navbar-brand" href="/">Block View</a>
              </nav>
            </header>
            <main>
              <Route path="/" exact component={BlocksListPage} />
              <Route path="/:date" exact component={BlocksListPage} />
              <Route path="/blocks/:id/:id_type(height|hash)" component={BlockDetailPage} />
              <Route path="/transactions/:id" component={TransactionDetailPage} />
            </main>
          </div> 
        </Router>
      </div>
    );
  }
}

export default App;
