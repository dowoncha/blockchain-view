import React from 'react'

import api from './ChainyardAPI'

/**
 * Block Detail Page
 * Fetch a sync block by either height or hash
 * /blocks/:id/:id_type(height|hash)
 */
class BlockDetailPage extends React.Component {
  constructor(props) {
    super(props)
    
    this.state = {
      block: undefined
    }
  }

  setStateAsync(state) {
    return new Promise((resolve, reject) => {
      this.setState(state, resolve)
    });
  }

  async componentDidMount() {
    let block;

    /*
    if (this.props.match.params.id_type === 'height')  {
      const height = this.props.match.params.id
      block = await api.fetchBlocksByHeight(height)

      console.log(block)
    } else */
    
    if (this.props.match.params.id_type === 'hash') {
      const hash = this.props.match.params.id
      block = await api.fetchBlockByHash(hash)
    } else {
      console.error("Id type must be height or hash in url params")
    }

    this.setStateAsync({ block })
  }

  render() {
    const { block } = this.state

    return (
      <div className="container">
        <h2>Block Detail</h2>
        <BlockDetailPanel block={block} />
        {block &&
          <section>
            <h2>Transactions</h2>
            <TransactionsTable transactions={block.tx}/>
          </section>
        }
      </div>
    );
  }
}

class BlockDetailPanel extends React.Component {
  render() {
    const { block } = this.props

    return (
      <div className="card">
        <div className="card-body">
          <h4 className="card-title">Details</h4>
          { block &&
            <div>
              <p className="text-left">Version: {block.ver}</p>
              <p className="text-left">Time Received: {block.time} - {}</p>
              <p className="text-left">Bits: {block.bits}</p>
              <p className="text-left">Fee: {block.fee}</p>
              <p className="text-left">Nonce: {block.nonce}</p>
              <p className="text-left"># Tx: {block.n_tx}</p>
              <p className="text-left">Size: {block.size}</p>
              <p className="text-left">Index: {block.block_index}</p>
              <p className="text-left">Is Main Chain: {block.main_chain}</p>
            </div>
          }
          <h4 className="card-title">Hashes</h4>
          { block &&
            (<div className="hashes">
              <p className="text-left">Hash: {block.hash}</p>
              <p className="text-left">Prev: {block.prev_block}</p>
            </div>)
          }
        </div>
      </div>
    );
  }
}

class TransactionsTable extends React.Component {
  constructor(props) {
    super(props);

    this.state = {
      currentPage: 1,
      txPerPage: 200 
    };
  } 

  goToPage = (event) => {
    this.setState({ currentPage: event.target.id })
  }

  render() {
    const { transactions } = this.props
    const { currentPage, txPerPage } = this.state

    // Block Paging logic
    const indexLastTx = currentPage * txPerPage;
    const indexFirstTx = indexLastTx - txPerPage;

    // Slice all transsactions into page 
    const currentTx = transactions.slice(indexFirstTx, indexLastTx);
      
    const transactionRows = currentTx.map(tx => (
      <TransactionTableRow key={tx.hash} tx={tx} />
    ));

    // TODO: move logic out into separate component
    // Page number logic
    // Crummy for now, should be easier way with map
    // [1, ..., maxPages]
    const pageNumbers = [];
    const maxPages = Math.ceil(transactions.length / txPerPage);
    for (let i = 1; i <= maxPages; i++) {
      pageNumbers.push(i)
    }

    const paginationComponents = pageNumbers.map(number => (
      <li key={number} className="page-item" onClick={this.goToPage}>
        <a className="page-link" id={number}>
          {number}
        </a>
      </li>
    ));

    return (
      <div>
        <table className="table table-hover table-sm">
          <thead>
            <tr>
              <td>Input</td>
              <td>Output</td>
              <td>Transaction Hash</td>
            </tr>
          </thead>
          <tbody>
            {transactionRows}
          </tbody>
        </table>
        <ul className="pagination">
          {paginationComponents}
        </ul>
      </div>
    );
  }
}

class TransactionTableRow extends React.Component {
  render() {
    const { tx } = this.props

    return (
      <tr>
        <td>
          {tx.inputs.length}
        </td>
        <td>
          {tx.out.length}
        </td>
        <td>
          <a href={`/transactions/${tx.hash}`}>{tx.hash}</a>
        </td>
      </tr>
    );
  }
}

export default BlockDetailPage

