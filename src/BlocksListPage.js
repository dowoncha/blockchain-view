import React, { Component } from 'react'

import api from './ChainyardAPI'
import moment from 'moment'

class BlockChainPage extends Component {
  constructor(props) {
    super(props);

    this.state = {
      blocks: []
    }
  }

  setStateAsync(state) {
    return new Promise((resolve, reject) => {
      this.setState(state, resolve)
    });
  }

  async componentDidMount() {
    // const block = await api.mockLatestBlock();
    // let { date = moment() } = this.props.match.params.date || moment()
    let date = moment()
    
    if (this.props.match.params.date) {
      date = this.props.match.params.date
    }

    // const blocks = [] 
    const date_in_ms = moment(date).valueOf()
    const { blocks } = await api.fetchBlocksByDate(date_in_ms)

    // const latestBlock = await api.fetchLatestBlock()

    // blocks.push(latestBlock)

    this.setStateAsync({ blocks });    
  }

  render() {
    let date = moment()
    if (this.props.match.params.hasOwnProperty('date')) {
      date = moment(this.props.match.params.date)
    }

    const yesterday = moment(date).subtract(moment.duration(1, 'days')).format("YYYY-MM-DD");
    const tomorrow = moment(date).add(1, 'days').format("YYYY-MM-DD");

    const { blocks } =  this.state
      
    return (
      <div className="container">
        <div>
          <h2>
            <span><a href={`/${yesterday}`}>&lt;&lt;---</a></span>
            <span>Blocks mined for {moment(date).format("MMM Do YY")}</span>
            <span><a href={`/${tomorrow}`}>---&gt;&gt;</a></span>
          </h2>  
        </div>
        <BlocksTable blocks={blocks} />
      </div>
    );
  }
}

class BlocksTable extends Component {
  constructor(props) {
    super(props);

    this.state = {
      currentPage: 1,
      blocksPerPage: 15 
    }
  }

  goToPage = (event) => {
    console.log(event.target)

    this.setState({ currentPage: event.target.id })
  }
    
  render() {
    const { blocks } = this.props
    const { currentPage, blocksPerPage } = this.state

    // Block Paging logic
    const indexLastBlock = currentPage * blocksPerPage
    const indexFirstblock = indexLastBlock - blocksPerPage

    const currentBlocks = blocks.slice(indexFirstblock, indexLastBlock);

    console.log(currentPage, indexFirstblock, indexLastBlock, currentBlocks)
      
    const blockRows = currentBlocks.map(block => <BlockTableRow key={block.hash} block={block} />);

    // Page number logic
    // Crummy for now, should be easier way with map
    // [1, ..., maxPages]
    const pageNumbers = [];
    const maxPages = Math.ceil(blocks.length / blocksPerPage);
    for (let i = 1; i <= maxPages; i++) {
      pageNumbers.push(i)
    }

    const paginationComponents = pageNumbers.map(number => (
      <li key={number} className="page-item" onClick={this.goToPage}>
        <a className="page-link" id={number}>
          {number}
        </a>
      </li>
    ));

    return (
      <div>
      <table className="table table-hover">
        <thead className="thead-dark">
          <tr>
            <th scope="col">Height</th>
            <th scope="col">Time Elapsed</th>
            <th scope="col">Hash</th>
          </tr>
        </thead>
        <tbody>
        {blockRows}
        </tbody>
      </table>
      <ul className="pagination">
        {paginationComponents}
      </ul>
      </div>
    );
  }
}

class BlockTableRow extends Component {
  render() {
    let { height, time, hash } = this.props.block

    return (
      <tr>
        <td>{height}</td>
        <td>{time}</td>
        <td><a href={`/blocks/${hash}/hash`} >{hash}</a></td>
      </tr>
    );
  }
}

export default BlockChainPage
