import React from 'react'
import moment from 'moment'
import api from './ChainyardAPI'

class TransactionDetailPage extends React.Component {
  constructor(props) {
    super(props);

    this.state = {
      transaction: undefined
    };
  }

  async componentDidMount() {
    const { id } = this.props.match.params;
    
    const transaction = await api.fetchTransaction(id);
    this.setStateAsync({ transaction });
  }

  setStateAsync(state) {
    return new Promise((resolve, reject) => {
      this.setState(state, resolve)
    });
  }

  render() {
    const { transaction } = this.state

    const transactionInputs = transaction && transaction.inputs.map(input => (
      <div className="list-group-item">
        <p>Script: {/*input.script*/}</p>
        {input.witness && (<p>Witness: {input.witness}</p>)}
        {input.prev_out && (
          <div>
            <h5>Previous Out</h5>
            <p>Addr: {input.prev_out.addr}</p>
            <p>Value: {input.prev_out.value}</p>
            <p>N: {input.prev_out.n}</p>
            <p>Script: {input.prev_out.script}</p>
          </div>
        )}
      </div>
    ));

    const transactionOuts = transaction && transaction.out.map(output => (
      <div className="list-group-item">
        <p>Addr: {output.addr}</p>
        <p>Value: {output.value}</p>
        <p>Type: {output.type}</p>
        <p>Script: {output.script}</p>
      </div>
    ));

    return (
      <div className="container">
        <h2>Transaction Detail</h2>
        { transaction && (
          <div className="card">
            <div className="card-body">
              <p className="text-left">Hash: {transaction.hash}</p>
              <p className="text-left">Index: {transaction.tx_index}</p>
              <p className="text-left">Time: {moment(transaction.time).format()}</p>
              <p className="text-left">Size: {transaction.size}</p>
              <p className="text-left">Weight: {transaction.weight}</p>
              <p className="text-left">Is Double Spend: {transaction.double_spend}</p>
            </div>
          </div>)
        }
        <div className="row">
          <div className="col-sm">
            <h4>Inputs</h4>
            <div className="list-group">
              {transactionInputs}              
            </div>
          </div>
          <div className="col-sm">
            <h4>Outputs</h4>
            <div className="list-group">
              {transactionOuts}
            </div>
          </div>
        </div>
      </div>
    );
  }
}

export default TransactionDetailPage
